# DoomDataset

Ce répertoire contient un set d'images classifiées issues de plusieurs sessions de Doom enregistrées.

## Contribuer au jeu de données 

### Prérequis

Requiert : OBS Studio et une version de Doom pour DOSBox, Adobe Photoshop et un logiciel de montage vidéo (Shotcut)

OBS : https://obsproject.com/fr/
GOG Doom : https://1fichier.com/?eb9fcxzj40eokpdsh35x 
Shotcut : https://shotcut.org/download/

### Configuration de OBS Studio et enregistrement du jeu

- Installer et lancer OBS Studio 
- En bas, dans Sources, appuyer sur le + et "Capture d'écran"
- Dans le menu Fichier allez dans "Paramètres" puis dans l'onglet "Vidéo", mettez la valeur "1" dans le champ "Valeur entières pour le débit d'images (FPS)"
- Une fois fait, en bas à droite appuyez sur "Démarrer l'enregistrement", une fois terminé appuyez de nouveau sur le bouton, la vidéo sera enregistrée dans le dossier spécifié dans les paramètres, onglet "Sortie" > "Chemin d'accès de l'enregistrement" (par défaut dans le dossier vidéo de votre ordinateur)

### Exportation de la vidéo en images individuelles

- Lancer Shotcut
- Glisser et déposer la vidéo du jeu enregistrée dans la timeline, retaillez là pour ne garder que des images du jeu
- Aller dans le menu "Fichier" > "Exporter" > "Vidéo"
- Dans le menu de sélection des formats sur la gauche dérouler, jusqu'en bas et sélectionner le format "JPEG"
- Appuyez sur le bouton "Avancé" et réglez "Images/sec" à 1.000
- Appuyez sur exporter, n'oubliez pas de prévoir un dossier prévu pour réceptionner les images, il y en aura beaucoup

## Retaillage des images sur Photoshop

### Préparation du script

La taille des images dans la base de données doit être unifiée, elle est de 320x240px
Pour cela nous utiliserons un script Photoshop pour traiter les images en lot

- Dans le dossier contenant toutes les captures, ajouter un dossier nommé "Processed"
- Ouvrir Photoshop et une première image
- Ouvrir le menu des actions dans "Fenêtre" > "Actions"
- Ajouter une nouvelle action (à côté de la poubelle) que vous nommerez comme vous voulez
- Rogner l'image s'il y a des bandes noires
- Aller dans le menu "Image" > "Taille de l'image"
- Réglez la hauteur et la largeur sur 320 par 240 (si c'est bien rogné ça le sera homotétiquement)
- Enregistrez l'image dans le dossier Processed en format JPEG qualité 12
- Appuyer sur le carré stop dans l'onglet action pour arrêter l'enregistrement du script

## Lancement de l'automatisation 

- Aller dans le menu "Fichier" puis "Automatisation, puis "Traitement par lots"
- Chercher l'action créée dans Ensemble et Actions
- Sélectionner le dossier Source, c'est le dossier contenant les captures d'écran
- A droite, dans destination, sélectionner le dossier "Processed" créé précédemment
- Appuyer sur OK
- Boire un bon café, un thé, ou fumer une cigarette
